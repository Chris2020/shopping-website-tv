import React from "react";

class Slider extends React.Component{
    state = {
        picArr : ['https://mobilecontent.costco.com/live/resource/img/ca-homepage/d-hero-200504-backyard-camp-en-r1.jpg',
        'https://mobilecontent.costco.com/live/resource/img/ca-homepage/d-hero-200505-clothing-en.jpg',
        'https://mobilecontent.costco.com/live/resource/img/ca-homepage/d-hero-200504-bathroom-en.jpg',
        'https://mobilecontent.costco.com/live/resource/img/ca-homepage/d-hero-200504-sheds-en.jpg',
        'https://mobilecontent.costco.com/live/resource/img/ca-homepage/d-hero-200504-LG-en.jpg'],
        count:0,
        enable : true,
    }
    num = 0
    constructor() {
        super();
        setInterval(() => {
            if (this.state.enable){
                this.setState({count : ++this.num % this.state.picArr.length})
            }
        },2000)
    }
    select(number){
        this.num = number
        this.setState({count:number})

    }
    startOrStop(){
        this.setState({enable:!this.state.enable})
    }
    render() {
       return(
           <div className="container">
               <div className="slider">
                   <img src={this.state.picArr[this.state.count]} alt=""/>
               </div>
               <div className="row justify-content-center">
                   {this.state.picArr.map((value,index) => {
                       return <button className="sliderBtn" onClick={event => this.select(index)} style={{background : this.state.count===index ? 'black' : ''}}></button>
                       // <span style={{margin:"2px",cursor:'pointer'}} onClick={event => this.select(index) }>
                           {/*<i className="far fa-circle" style={{background : this.state.count===index ? 'black' : ''}}></i></span>*/}
                   })}
                   <div className="align-item-center" style={{width:'20px',height:'20px',margin: '10px',marginTop:'8px'}} onClick={event => this.startOrStop()}>
                           {this.state.enable === true ? <i className="fas fa-pause" style={{cursor:'pointer'}}></i> : <i className="fas fa-play"style={{cursor:'pointer'}}></i>}
                   </div>
               </div>
           </div>
       )
    }
}

export default Slider