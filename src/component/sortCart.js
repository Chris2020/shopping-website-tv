import React from "react";
import "./tv.css"
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {actFetchTvList, sortByPrice} from "../actions";

class SortCart extends React.Component{
    state = {
        sortWay : '1'
    }
    render() {
        if (this.state.sortWay === '2'){
            this.props.sortByPrice()
        }else if (this.state.sortWay === '1'){
            this.props.actFetchTvList()
        }

       return(
           <div className="sortCartDiv">
               <div>
                   Sort by:
                   <select id="sort" value={this.state.sortWay} onChange={event => this.setState({sortWay:event.target.value})}>
                       <option value="1">Best Match</option>
                       <option value="2">Price Low to High</option>
                       <option value="3">Size Low to High</option>
                       <option value="4">Size High to Low</option>
                       <option value="5">Rating Low to High</option>
                       <option value="6">Rating Low to High</option>
                   </select>
               </div>
               <div><Link to="/cart"><i className="fas fa-shopping-cart"></i></Link></div>
           </div>
       )
    }
}
export default connect(null,{sortByPrice,actFetchTvList}) (SortCart)