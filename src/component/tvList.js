import React from "react";
import {connect} from "react-redux";
import {actFetchSingleTv, actFetchTvList} from "../actions";
import {Link} from "react-router-dom";
import "./tv.css"

class TvList extends React.Component{
    state = {
        tvListState : [],
    }
    componentDidMount() {
       this.props.actFetchTvList();
    }
    showInfo (id) {
        this.props.actFetchSingleTv(id)
    }
    render() {
       return (
           <div className="showTvs m-3">
               {
                   this.props.tvList?.map((value,index) => {
                       return (
                               <div className="card m-4" style={{width: '18rem'}} key={index}>
                                   <img src={value.piclink[0]} className="card-img-top" alt="..."></img>
                                       <div className="card-body">
                                           <h5 className="card-title">
                                               {`${value.brand} ${value.size} ${value.price}`}
                                               <Link to='/showInfo'> <button type="button" className="btn btn-warning float-right"
                                                       onClick={event => this.showInfo(value.id)}
                                               >Info</button></Link>

                                           </h5>
                                           <p className="card-text">{value.description}</p>
                                           <p className="card-text">
                                               {[...Array(value.rating)].map((star,index) => <i className="fas fa-star" style={{color : 'goldenrod'}} key={index}></i>)}
                                           </p>
                                           {/*<a href="#" className="btn btn-primary">Go somewhere</a>*/}
                                       </div>
                               </div>
                       )
                   })
               }
           </div>
       )
    }
}
const mapStateToProps = state => {
    // console.log(state)
    return {
        tvList : state.tvListReducer.tvList
    }
}

export default connect(mapStateToProps,{actFetchTvList,actFetchSingleTv}) (TvList)