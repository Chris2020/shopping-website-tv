import React from "react";
import "./tv.css"
import axios from "axios"

class SearchBar extends React.Component{


    render() {
       return(
       <div className="searchBar">
           <div>
               <span className="searchBtn">Search</span>
           </div>
           <div className="searchInputArea">
               <input type="text" name="searchBar" id="" className="searchInput"/>
           </div>
       </div>)
    }

}
export default SearchBar