import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import "./tv.css"
import {actGetTvAmount, actShoppingInfo} from "../actions";

class ShowInfo extends React.Component{
    state = {
        picNum : 0,
        amount : 0,
        display : false,
    };
    componentDidMount() {
        console.log(this.props.tvSingle)
    }
    sendShoppingInfo([obj,amount]){
        this.props.actShoppingInfo([obj,amount])
        this.setState({display:!this.state.display})
        setTimeout(() => {this.setState({display:!this.state.display})},4000)
    }
    render() {
        console.log(this.props.amount)
        return (
            <div className="containerShow">
                <div className="shoppingSubmit" style={this.state.display === true ? {visibility:'visible'}:{visibility:'hidden'}}>
                    <label style={{fontWeight:'bold'}}>Shopping Cart</label>
                    <p>Item add successful!</p>
                    <Link to="/cart">
                        <button className="btn btn-primary">Go to cart</button>
                    </Link>
                </div>
                <div className="showInfo">
                    <div className="col-sm-6">
                        <div>
                            <img src={this.props.tvSingle[0]?.piclink[this.state.picNum]} alt=""
                                 className="card-img-top"
                            />
                        </div>
                        <div className="smallPic">
                            {this.props.tvSingle[0]?.piclink.map((value, index) => {
                                return <img src={value[0]} alt="" key={index}
                                            style={{width: "110px", height: "110px"}}
                                            onClick={event => this.setState({picNum: index})}
                                />
                            })}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="infoArea">Information</div>
                        <div className="infoCard">
                            <div className="card" style={{width: '80%'}}>
                                <div className="card-body">
                                    <h5 className="card-title">{this.props.tvSingle[0]?.brand}</h5>
                                    {/*<h6 className="card-subtitle mb-2 text-muted"></h6>*/}
                                    <p className="card-text">{this.props.tvSingle[0]?.description}</p>
                                    <p className="card-text">
                                        {[...Array(this.props.tvSingle[0]?.rating)].map((star, index) => <i
                                            className="fas fa-star" style={{color: 'goldenrod'}} key={index}></i>)}
                                    </p>
                                    <h5 className="card-title">${this.props.tvSingle[0]?.price}</h5>
                                    {/*<a href="#" className="card-link">Card link</a>*/}
                                    {/*<a href="#" className="card-link">Another link</a>*/}
                                    <div className="cartDiv">
                                        <div className="col-4">
                                            <input type="number" className="numInput"
                                                   onChange={event => this.setState({amount: parseInt(event.target.value) })}
                                            />
                                        </div>
                                        <div className="col-8">
                                            <button type="button" className="btn btn-primary"
                                                    onClick={event => this.sendShoppingInfo([this.props.tvSingle, this.state.amount])}
                                            >Add to Cart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flex-direction-column">
                            <div style={{width:'80%'}}>
                                <Link to='/'>
                                    <button type="button" className="btn btn-warning" style={{width:'100%'}}>Back</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        tvSingle : state.tvListReducer.tvSingle,
    }
};
export default connect(mapStateToProps,{actShoppingInfo}) (ShowInfo)