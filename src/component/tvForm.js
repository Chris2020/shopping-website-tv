import React from "react";
import "./tv.css"
import axios from "axios";
import {Field,reduxForm} from "redux-form";


class TvForm extends React.Component{
    state = {
        pics : []
    }
    componentDidMount() {
        axios.get('http://localhost:3000/posts')
            .then(res => {
                console.log(res.data)
                this.setState({pics : res.data})
            }).catch(error => {
            console.log(error)
        })
    }
    render() {
       return (
           <div className="tvFormDiv">
               <div className="headerInput">TvInput</div>
               <div className="formCard">
                   <form>
                       <div className="form-row">
                           <div className="form-group col-md-6">
                               <label htmlFor="inputEmail4">Brand</label>
                               {/*<input type="email" className="form-control" id="inputEmail4"></input>*/}
                               <Field type="text" className="form-control" name="brand" component='input'></Field>
                           </div>
                           <div className="form-group col-md-6">
                               <label htmlFor="inputPassword4">Size</label>
                               <input type="password" className="form-control" id="inputPassword4"></input>
                               {/*<Field type="text" className="form-control" name="size"></Field>*/}
                           </div>
                       </div>
                       <div className="form-group">
                           <label htmlFor="inputAddress">Description</label>
                           <input type="text" className="form-control" id="inputAddress"
                                  placeholder=""></input>
                           {/*<Field type="text" className="form-control" name="description"></Field>*/}
                       </div>
                       <div className="form-group">
                           <label htmlFor="inputAddress2">Pic Link</label>
                           <input type="text" className="form-control" id="inputAddress2"
                                  placeholder=""></input>
                           {/*<Field type="text" className="form-control" name="picLink"></Field>*/}
                       </div>
                       <div className="form-row">
                           <div className="form-group col-md-6">
                               <label htmlFor="inputCity">Price</label>
                               <input type="text" className="form-control" id="inputCity"></input>
                               {/*<Field type="text" className="form-control" name="price"></Field>*/}
                           </div>
                           <div className="form-group col-md-4">
                               <label htmlFor="inputState">Made of</label>
                               <select id="inputState" className="form-control">
                                   <option selected>Choose...</option>
                                   <option>...</option>
                               </select>
                           </div>
                           <div className="form-group col-md-2">
                               <label htmlFor="inputZip">Ratings</label>
                               <input type="text" className="form-control" id="inputZip"></input>
                               {/*<Field type="text" className="form-control" name="rating"></Field>*/}
                           </div>
                       </div>
                       {/*<div className="form-group">*/}
                       {/*    <div className="form-check">*/}
                       {/*        <input className="form-check-input" type="checkbox" id="gridCheck"></input>*/}
                       {/*        <label className="form-check-label" htmlFor="gridCheck">*/}
                       {/*            Check me out*/}
                       {/*        </label>*/}
                       {/*    </div>*/}
                       {/*</div>*/}
                       <button type="submit" className="btn btn-primary">Add Tv</button>
                   </form>
               </div>
               <div className="tvContainer">
                   {this.state.pics.map((value,index) => {
                       return (
                           <div className="card m-4" style={{width: '18rem'}} key={index}>
                               <img src={value.piclink} className="card-img-top" alt="..."></img>
                               <div className="card-body">
                                   <h5 className="card-title">
                                       {`${value.brand} ${value.size} ${value.price}`}
                                   </h5>
                                   <p className="card-text">{value.description}</p>
                                   <p className="card-text">
                                       {[...Array(value.rating)].map((star,index) => <i className="fas fa-star" style={{color : 'goldenrod'}} key={index}></i>)}
                                   </p>
                               </div>
                           </div>
                       )
                   })}
               </div>
           </div>
       )
    }
}
export default reduxForm({
    form : 'tvForm'
})(TvForm)
