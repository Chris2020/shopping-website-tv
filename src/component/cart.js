import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {actRemoveTv} from "../actions";

class Cart extends React.Component{
    state = {
        total : 0,
    }
    totalPrice=0
    onDelete(id){
        this.props.actRemoveTv(id)
        this.totalPrice = 0
    }
    render() {
        return (
            <div className="container">
                <div className="cartTitle">Shopping Cart</div>
                <div style={{margin:'20px',padding:'10px'}}>
                    <div className="row align-items-center">
                        <div className="col col-md-2 align-items-center">PRODUCT</div>
                        <div className="col col-md-2 align-items-center">NAME OF PRODUCT</div>
                        <div className="col col-md-2 align-items-center">PRICE</div>
                        <div className="col col-md-2 align-items-center">QUANTITY</div>
                        <div className="col col-md-2 align-items-center">REMOVE</div>
                        <div className="col col-md-2 align-items-center">TOTAL</div>
                    </div>
                    {this.props.shoppingInfo?.length === 0 ? <div className="emptyCart"><h2>Your cart is empty</h2></div> : ''}
                    {this.props.shoppingInfo?.map(value => {
                        this.totalPrice = this.totalPrice +  value[0][0].price * value[1]
                        return (
                            <div className="row align-items-center mb-3" style={{height:"100px"}}>
                            <div className="col"><img src={value[0][0].piclink[0]} alt="" style={{width:"100%"}}/></div>
                            <div className="col">{value[0][0].description}</div>
                            <div className="col">{value[0][0].price}</div>
                            <div className="col">{value[1]}</div>
                            <div className="col"><i className="fas fa-trash-alt btn btn-danger"
                                                    onClick={event => this.onDelete(value[0][0].id)}
                            ></i></div>
                            <div className="col">${value[0][0].price * value[1]}</div>
                        </div>)

                    })}
                    <br/>
                    <div className="row m-3 justify-content-end">
                        <div className="col-2">Total:${this.totalPrice}</div></div>
                    <div className="row"><Link to="/"><button type="button" className="btn btn-primary">Back</button></Link></div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        shoppingInfo: state.tvListReducer.shoppingInfo,
    }
};
export default connect(mapStateToProps,{actRemoveTv}) (Cart)