import {combineReducers} from "redux";
import {LIST_ALL, LIST_ONE, REMOVE_TV, SHOPPING_INFO, SORT_PRICE} from "../helper";
import {reducer} from "redux-form";

const addReducers = (state = 0,action) => {
    if (action.type === 'add'){
        return state + 1
    }
    else {
        return state
    }
}
const initialState = {
    tvList : [],
    tvSingle : {},
    shoppingInfo:[],
};

const tvListReducer = (state = initialState,action) => {
    switch (action.type) {
        case LIST_ALL:
            return {...state,tvList: [...action.payload]};
        case SORT_PRICE:
            return {...state,tvList: action.payload.sort(function (a,b) {
                    return (a.price - b.price)
                }) }
        case LIST_ONE:
            return {...state,tvSingle:{...action.payload}}
        case SHOPPING_INFO:
                if (state.shoppingInfo.length > 0){
                   let typeCart = 0
                   state.shoppingInfo.find(value => action.payload[0][0].id === value[0][0].id ? typeCart = 1 : typeCart = 0)
                    if (typeCart === 1){
                      let tempCart = state.shoppingInfo.map((value) =>
                          {
                              if (value[0][0].id === action.payload[0][0].id){
                                  value[1] = value[1] + action.payload[1]
                                  return value
                              }else{
                                  return value
                              }
                          }
                        )
                        return {...state,shoppingInfo: tempCart}
                    }else {
                        return {...state,shoppingInfo: [...state.shoppingInfo,action.payload]}
                    }
                }else {
                    return {...state,shoppingInfo: [...state.shoppingInfo,action.payload]}
                }
        case REMOVE_TV:
            return {...state,shoppingInfo: state.shoppingInfo.filter(x => x[0][0].id !== action.payload)}


            // state.shoppingInfo.map(value => value[0][0].id !== action.payload[0][0].id ? value : {...value,}
        default:
            return state

    }
}
export default combineReducers({
    addReducers,
    tvListReducer,
    form : reducer,
})