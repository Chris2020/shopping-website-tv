import tvApi from "../api/tvApi";
import {LIST_ALL, LIST_ONE, REMOVE_TV, SHOPPING_INFO, SORT_PRICE} from "../helper";

// const actCalculateAdd = () => {
//     return {
//         type : 'add',//这个是必须有的，每个action的type值是唯一的
//     }
// };

const actFetchTvList = () => async dispatch => {
    try {
        let res = await tvApi.get('0')
        dispatch({type : LIST_ALL,payload : res.data})
    } catch (e) {
       console.log(e)
       return ''
    }
}
const actFetchSingleTv = (id) => async dispatch => {
    try {
        let res = await tvApi.get(`${id}`)
        dispatch({type : LIST_ONE,payload: res.data})
    }catch (e) {
       console.log(e)
       return ''
    }
}
const actShoppingInfo = value => {
    return {
        type : SHOPPING_INFO,
        payload : value,
    }
}
const actRemoveTv = id => {
    return {
        type : REMOVE_TV,
        payload : id,
    }
};
const sortByPrice = () => dispatch => {
    // try {
    //     let res = await tvApi.get('0')
    //     dispatch({type : SORT_PRICE ,payload : res.data})
    // } catch (e) {
    //     console.log(e)
    //     return ''
    // }
   tvApi.get('0')
       .then(res => {
           dispatch({type : SORT_PRICE,payload : res.data})
       }) .catch(err => {
           console.log(err)
            dispatch({type : null})
   })

}
export {
    actFetchTvList,
    actFetchSingleTv,
    actShoppingInfo,
    actRemoveTv,
    sortByPrice,
}