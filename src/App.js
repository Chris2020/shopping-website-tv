import React from 'react';
import TvList from "./component/tvList";
import ShowInfo from "./component/showInfo";
import {BrowserRouter,Route} from "react-router-dom";
import TvForm from "./component/tvForm";
import Slider from "./component/slider";
import "./component/tv.css"
import SortCart from "./component/sortCart";
import Cart from "./component/cart";
import SearchBar from "./component/searchBar";

const HomePage = () => {
    return (
        <div className="container">
            {/*<TvForm></TvForm>*/}
            <SearchBar></SearchBar>
            <Slider></Slider>
            <SortCart></SortCart>
            <TvList></TvList>

        </div>
    )
}
function App() {
  return (
    <div>
        {/*<TvList></TvList>*/}
        {/*<ShowInfo></ShowInfo>*/}
        <BrowserRouter>
          <Route path='/' exact component={HomePage}></Route>
          <Route path='/showInfo' exact component={ShowInfo}></Route>
            <Route path='/cart' component={Cart}></Route>
            <Route path='/tvForm' component={TvForm}></Route>
        </BrowserRouter>
    </div>
  );
}

export default App;
